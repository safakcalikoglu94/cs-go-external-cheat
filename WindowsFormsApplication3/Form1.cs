﻿using System;
using System.Collections.Generic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using Trolling;

namespace WindowsFormsApplication3
{
    public partial class Form1 : Form
    {
        public static int Client;
        public static Module clientModule = new Module("client_panorama.dll");
        public static Module engineModule = new Module("engine.dll");
        public static Process p = Process.GetProcessesByName("csgo")[0];
        public static int playerAdress;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
           
            this.Text = nameChange();
            Thread antiflash = new Thread(new ThreadStart(dwordAntiFlash));
            antiflash.Start();

        }
        public static string nameChange()
        {
                Random random = new Random();
                const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                return new string(Enumerable.Repeat(chars, 10).Select(s => s[random.Next(s.Length)]).ToArray());

        }

        public static float flashMaxAlpha
        {
            get
            {
                int BaseAddress = Class_Memory.ReadInt((int)clientModule.moduleAddress + signatures.dwLocalPlayer);
                return Class_Memory.ReadFloat((int)BaseAddress + offsets.m_flFlashMaxAlpha);
            }
            set
            {
                int BaseAddress = Class_Memory.ReadInt((int)clientModule.moduleAddress + signatures.dwLocalPlayer);
                Class_Memory.WriteFloat((int)BaseAddress + offsets.m_flFlashDuration, (float)value);
                Class_Memory.WriteFloat((int)BaseAddress + offsets.m_flFlashMaxAlpha, (float)value);
            }
        }

        public static void dwordAntiFlash()
        {
            if (engineModule.moduleAddress != 0x000000)
            {
                playerAdress = Class_Memory.ReadInt(clientModule.moduleAddress + signatures.dwLocalPlayer);
                int BaseAddress = Class_Memory.ReadInt((int)clientModule.moduleAddress + signatures.dwLocalPlayer);
                float flashVal = Class_Memory.ReadFloat((int)BaseAddress + offsets.m_flFlashMaxAlpha);
                while (true)
                {
                    int EngineBase = Class_Memory.ReadInt(engineModule.moduleAddress + signatures.dwClientState);
                    int GameState = Class_Memory.ReadInt(EngineBase + signatures.dwClientState);
                    if (HackStatus.antiflashStatus==true)
                    {
                        if (GameState == 6)
                        {
                            flashMaxAlpha = 0f;
                            Thread.Sleep(350);
                        }
                    }
                    else
                    {
                        flashMaxAlpha = 255f;
                    }
                }
            }
        }
        public class HackStatus
        {
            public static bool antiflashStatus = false;
        }

        private void flashButton_CheckedChanged(object sender, EventArgs e)
        {
            {
                if (noFlash.Checked == true)
                {
                    HackStatus.antiflashStatus = true;
                }
                else
                {
                    HackStatus.antiflashStatus = false;
                }
            }
        }

        public struct Module
        {
            public string moduleName;
            public int moduleAddress;

            public Module(string moduleName_)
            {
                
                moduleName = moduleName_;
                moduleAddress = 0x000000;
                try
                {
                    Process[] p = Process.GetProcessesByName("csgo");
                

                    if (p.Length > 0)
                    {

                        foreach (ProcessModule m in p[0].Modules)
                        {
                            if (m.ModuleName == moduleName_)
                            {
                                moduleAddress = (Int32)m.BaseAddress;

                            }
                        }


                    }
                    else
                    {
                        MessageBox.Show("csgo kapalı");

                    }

                }
                catch (Exception ex)
                {

                  MessageBox.Show(ex.Message);
                }

            }

        }

    }
}
